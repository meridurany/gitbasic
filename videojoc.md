# Videojoc
- Aquest document contindrà la definició del joc.
# Breu història
Situada en un arxipèlag sense concretar del Carib i en una època 
indefinida la història comença quan el protagonista, Guybrush 
Threepwood, arriba a l'illa de Mêlée amb l'aspiració d'esdevenir un 
pirata.

Allà, busca els líders dels pirates que li demanen superar tres proves 
per a poder convertir-se en un d'ells, la primera prova consisteix a 
vèncer la mestra de l'espasa, Carla, en un duel d'insults, la segonaa en 
trobar un tresor enterrat i la tercera en robar una figura de la casa de 
la governadora de l'illa, Elaine Marley, de qui s'enamora immediatament.

# Sistema de joc
El sistema de joc consisteix en una pantalla dividida en dues parts 
principals, la superior conté els gràfics i el cursor, i la inferior una 
sèrie de menús i botons que permeten realitzar accions i combinar 
elements que hem anat adquirint durant el joc.

![Guybrush Threepwood](Guybrush_Threepwood.png)

Es tracta d'un joc en tercera persona en què veiem constantment en 
pantalla el protagonista, en Guybrush Threepwood, a qui dirigim.
